#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

//This is the structure of the file in fat32 file
struct DirectoryEntry
{
    char DIR_Name[11];
    uint8_t DIR_Attr;
    uint8_t Unused1[8];
    uint16_t DIR_FirstClusterHigh;
    uint8_t Unused2[4];
    uint16_t DIR_FirstClusterLow;
    uint32_t DIR_FileSize;
};

//We are going to read only 16 directory entry from the address
//So it is always good to have only 16 files under one directory
struct DirectoryEntry Dir[16];

/*printing the information of the specified direcotry index*/
//It will print File Attribute, Starting Cluster NUmber and Size of the file
static void print_info(int i)
{
   // printf("Name:                          %s\n", Dir[i].DIR_Name);
    printf("File Attribute\t\tSize\t\tStarting Cluster Number\n%d\t\t\t%d\t\t%d\n", Dir[i].DIR_Attr, Dir[i].DIR_FileSize, Dir[i].DIR_FirstClusterLow);
}


/* This function will fill the entries of the direcotry from the given address*/
static void fill_directories(FILE **fp,int address)
{
    int i = 0;
    for(i = 0; i < 16; i++)
    {
        fseek( (*fp), address+0x00, SEEK_SET);
        fread( Dir[i].DIR_Name, 1, 11, (*fp));
        
       

        fseek( (*fp), (address+0x0B), SEEK_SET);
        fread( &(Dir[i].DIR_Attr), 1, 1, (*fp));
 
        fseek( (*fp), address+0x14, SEEK_SET);
        fread( &(Dir[i].DIR_FirstClusterHigh), 2, 1, (*fp));
   
        fseek( (*fp), address+0x1A, SEEK_SET);
        fread( &(Dir[i].DIR_FirstClusterLow), 2, 1, (*fp));
   
        fseek( (*fp), address+0x1C, SEEK_SET);
        fread( &(Dir[i].DIR_FileSize), 4, 1, (*fp));
 
        address = address+sizeof(struct DirectoryEntry);
      }   
}

/*  This function is going to print the valid available directory or file in the file 
    It will first fill the directory entry from the given address as a parameter from the file
    using file pointer to help. It will use fill directories function.
    and then print the valid directories 
*/
static void ls_function(FILE **fp, int address)
{
    fill_directories(fp, address);
     
    int i = 0 ;
    for( i = 0; i<16; i++)
    {
        if( Dir[i].DIR_Attr == 0x01 ||  Dir[i].DIR_Attr == 0x10 || Dir[i].DIR_Attr == 0x20)
        { 
                if( (int)(Dir[i].DIR_Name[0]) != 0xffffffe5 && (int)(Dir[i].DIR_Name[0]) != 0xffffff05)
                {
                   if( Dir[i].DIR_Attr == 0x10 )
                   {
                        char a[8];
                        int j;
                        for( j = 0; j<8;j++){a[j]= Dir[i].DIR_Name[j];}
                        printf("%s\n",a);
                   }
                    else
                   {
                       printf("%s\n",Dir[i].DIR_Name);
                   }
                }
        }    
    }     
     
}

/* This is stat function to show the stats of the directory */
/* It will first fill the entries in the directories.
   This function will remove all the spaces and dot from the string parameter
   i.e. "stat" and make it uppercase.
   After cleaning "stat" string it will also remove the space from each file name and compare
   to the "stat", and if the "stat" matches with the filename then it will print the info 
   of that file or the directory. 
   So over here "Bar.txt" is same as "BARTXT" or "bart.xt" is same as "bartxt"
*/
//If directory not found then it will return 0, 
//which is an error signal for file or directory not found

static int stat_function(FILE **fp,int address,char stat[])
{
     fill_directories(fp, address);
   
     int found = 0;
     /*making stat string in uppercase*/
     int len = strlen(stat);
     
     int i = 0 ;
     int j = 0 ;
     char* stat_copy = (char*) malloc(sizeof(char)*len);
     for(i = 0; i < len; i++)
     {
          int char_value = (int)(stat[i]);
          if( char_value > 96 && char_value < 123)
          {
             stat_copy[j] = (char)(char_value-32); j++;
          }
          else if(stat[i] != '.')
          {
              stat_copy[j] = stat[i];j++;
          }
     } 
     for( j = j; j<len; j++){stat_copy[j] = '\0';}
     //printf("%s\n",stat_copy);
     for( i =0; i < 16; i++)
     {
           int dir_len = 11;
           if(Dir[i].DIR_Attr == 0x10)
           {
                int k;int lm=0;
                char* name_copy = (char*) malloc(sizeof(char)*8);
                for( k = 0; k < 8; k++)     
                {
                   if(Dir[i].DIR_Name[k] != '.' && Dir[i].DIR_Name[k] != ' ')
                   {
                        name_copy[lm] = Dir[i].DIR_Name[k];lm++;
                   }
                }
                for( lm = lm; lm<8; lm++){name_copy[j] = '\0';}
                if(!strcmp(stat_copy, name_copy))
                {
                   found = 1;
                   print_info(i);
                   break;
                }
                free(name_copy);
           }
           else
           {
                char* name_copy = (char*) malloc(sizeof(char)*dir_len);
                int k;int lm=0;
                for(k = 0; k < dir_len; k++)
                {
                   int char_value = (int)(Dir[i].DIR_Name[k]);
                   if( char_value > 96 && char_value < 123)
                   {
                       name_copy[lm] = (char)(char_value-32); lm++;
                   }
                   else if(Dir[i].DIR_Name[k] != '.' && Dir[i].DIR_Name[k] != ' ')
                   {
                        name_copy[lm] = Dir[i].DIR_Name[k];lm++;
                   }
                }
                for( lm = lm; lm<dir_len; lm++){name_copy[j] = '\0';}
                if(!strcmp(stat_copy, name_copy))
                {
                   found = 1;
                   print_info(i);
                   break;
                }
                free(name_copy);
           }
     }
     free(stat_copy);
     return found;
}

//Time to find the directory
//This function will use the same startegy as "stat_function" but instead of printing out
//info of the direcotry it will return the starting cluster number of the directory
//It will only compare string with directories not with file name.
//It is given that each directory has "0x10" as an attribute
//If directory not found then it will return -999, which is an error signal in this program

static int get_stat(FILE** fp, int address,char string[])
{
     fill_directories(fp, address);
     /*making stat string in uppercase*/
     int len = strlen(string);
     
     int i = 0 ;
     int j = 0 ;
     char* string_copy = (char*) malloc(sizeof(char)*len);
     for(i = 0; i < len; i++)
     {
          int char_value = (int)(string[i]);
          if( char_value > 96 && char_value < 123)
          {
             string_copy[j] = (char)(char_value-32); j++;
          }
          else if(string[i] != '.')
          {
              string_copy[j] = string[i];j++;
          }
     } 
     for( j = j; j<len; j++){string_copy[j] = '\0';}
    
    int found = -999;
    for( i = 0; i<16;i++)
    {
        if(Dir[i].DIR_Attr == 0x10 )
        {
              int k;int lm=0;
              char* name_copy = (char*) malloc(sizeof(char)*8);
              for( k = 0; k < 8; k++)     
              {
                   if(Dir[i].DIR_Name[k] != '.' && Dir[i].DIR_Name[k] != ' ')
                   {
                         name_copy[lm] = Dir[i].DIR_Name[k];lm++;
                   }
              }
              for( lm = lm; lm<8; lm++){name_copy[j] = '\0';}
              
              if(!strcmp(string_copy, name_copy))
              {
                  found = Dir[i].DIR_FirstClusterLow;
                  free(name_copy);
                  free(string_copy);
                  return found;
              }
              free(name_copy);
        }
    }
    free(string_copy);
    return found;
}

//Time to read some data from txt files
//This function will use the same startegy as "stat_function" but instead of printing out
//info of the file it will return the starting cluster number of the file
//It will only compare string with only files not with directories name.
//It is given that each directory has "0x10" as an attribute
//If file not found then it will return -999, which is an error signal in this program

static int data_stat(FILE **fp,int address,char stat[])
{
     fill_directories(fp, address);
   
     int found = -999;
     /*making stat string in uppercase*/
     int len = strlen(stat);
     
     int i = 0 ;
     int j = 0 ;
     char* stat_copy = (char*) malloc(sizeof(char)*len);
     for(i = 0; i < len; i++)
     {
          int char_value = (int)(stat[i]);
          if( char_value > 96 && char_value < 123)
          {
             stat_copy[j] = (char)(char_value-32); j++;
          }
          else if(stat[i] != '.')
          {
              stat_copy[j] = stat[i];j++;
          }
     } 
     for( j = j; j<len; j++){stat_copy[j] = '\0';}
     
     for( i =0; i < 16; i++)
     {
           int dir_len = 11;
           if(Dir[i].DIR_Attr != 0x10)
           {
                char* name_copy = (char*) malloc(sizeof(char)*dir_len);
                int k;int lm=0;
                for(k = 0; k < dir_len; k++)
                {
                   int char_value = (int)(Dir[i].DIR_Name[k]);
                   if( char_value > 96 && char_value < 123)
                   {
                       name_copy[lm] = (char)(char_value-32); lm++;
                   }
                   else if(Dir[i].DIR_Name[k] != '.' && Dir[i].DIR_Name[k] != ' ')
                   {
                        name_copy[lm] = Dir[i].DIR_Name[k];lm++;
                   }
                }
                for( lm = lm; lm<dir_len; lm++){name_copy[j] = '\0';}
                if(!strcmp(stat_copy, name_copy))
                {
                   found = Dir[i].DIR_FirstClusterLow;
                   free(name_copy);
                   free(stat_copy);
                   return found;
                }
                free(name_copy);
           }
     }
     free(stat_copy);
     return found;
}

static int file_size_stat(FILE **fp,int address,char stat[])
{
     fill_directories(fp, address);
   
     int found = 0;
     /*making stat string in uppercase*/
     int len = strlen(stat);
     
     int i = 0 ;
     int j = 0 ;
     char* stat_copy = (char*) malloc(sizeof(char)*len);
     for(i = 0; i < len; i++)
     {
          int char_value = (int)(stat[i]);
          if( char_value > 96 && char_value < 123)
          {
             stat_copy[j] = (char)(char_value-32); j++;
          }
          else if(stat[i] != '.')
          {
              stat_copy[j] = stat[i];j++;
          }
     } 
     for( j = j; j<len; j++){stat_copy[j] = '\0';}
     
     for( i =0; i < 16; i++)
     {
           int dir_len = 11;
           if(Dir[i].DIR_Attr != 0x10)
           {
                char* name_copy = (char*) malloc(sizeof(char)*dir_len);
                int k;int lm=0;
                for(k = 0; k < dir_len; k++)
                {
                   int char_value = (int)(Dir[i].DIR_Name[k]);
                   if( char_value > 96 && char_value < 123)
                   {
                       name_copy[lm] = (char)(char_value-32); lm++;
                   }
                   else if(Dir[i].DIR_Name[k] != '.' && Dir[i].DIR_Name[k] != ' ')
                   {
                        name_copy[lm] = Dir[i].DIR_Name[k];lm++;
                   }
                }
                for( lm = lm; lm<dir_len; lm++){name_copy[j] = '\0';}
                if(!strcmp(stat_copy, name_copy))
                {
                   found = Dir[i].DIR_FileSize;
                   free(name_copy);
                   free(stat_copy);
                   return found;
                }
                free(name_copy);
           }
     }
     free(stat_copy);
     return found;
}
